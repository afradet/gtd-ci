FROM docker:20.10.16
ENV CYPRESS_CACHE_FOLDER=/tmp/.cache
RUN apk update && \
    apk upgrade --no-cache && \
    apk add --no-cache openssl libgcc libstdc++ ncurses-libs && \
    apk add --no-cache maven
RUN docker -v
RUN mvn -v
